<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Main Student</title>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #ffff;
        text-align: center;
    }

    h1 {
        text-align: center;
        margin-top: 20px;
    }
    table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }
    th, td {
        border: 1px solid #ccc;
        padding: 8px;
        text-align: center;
    }

    th {
        background-color: #f2f2f2;
    }

    td {
        background-color: #fff;
    }

    #checkButton {
        display: block;
        margin: 20px auto;
        padding: 10px 20px;
        font-size: 16px;
        border: 2px solid #4CAF50;
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    #checkButton:disabled {
        background-color: #ccc;
        border-color: #ccc;
        cursor: not-allowed;
    }

    #checkButton:hover {
        background-color: #45a049;
    }
    .TOP1,.TOP2,.TOP3,.DL1,.DL2,.DL3,.BA1,.BA2,.BA3 {
        background-color: #f4f4f4;
        display: none; /* ซ่อนภาพเริ่มต้น */
        margin: 20px auto;
        width: 200px;
    }
</style>
</head>
<body>
<h1><%=request.getAttribute("coursecode") %></h1>
    <table border="1" id="studentsocreTable">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
                <th>Attendance</th>
                <th>HW01</th>
                <th>Lab01</th>
                <th>MidLec</th>
                <th>MidLab</th>
                <th>HW02</th>
                <th>Lab02</th>
                <th>FinalLec</th>
                <th>FinalLab</th>
                <th>SUM</th>
                <th>Status</th>
                <th>DL</th>
                <th>BA</th>
            </tr>
        </thead>
        
        <tbody>
            <% 
                List<String> data = (List<String>) request.getAttribute("rowDataList");  
                if (data != null && !data.isEmpty()) {
                    
                    	 %>
                         <tr> 
                        <%  for (int i =0;i<=15;i++) {   %>                 
                             <td><%= data.get(i) %></td> 
                             <% 
                                 }
                             %>
                         </tr>
             <% 
                 } 
                  else {
            %>
            <tr>
                <td colspan="21">No data available</td>
            </tr>
            <% 
                }
            %>
        </tbody>
    	</table>
<button id="checkButton" disabled>Check</button>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        // กำหนดค่าตัวแปร datastatus จาก Java code
        var datastatus = <%= data.get(13) %>;
        if (datastatus == 1) {
            $('#checkButton').prop('disabled', false);
        }

        $('#checkButton').click(function() {
            var logincode = '<%= request.getParameter("logincode") %>';
            var coursecode = '<%= request.getParameter("coursecode") %>';
            var teachercode = '<%= request.getParameter("teachercode") %>';
            var status = true;
            var url = "check_std?logincode=" + logincode + "&coursecode=" + coursecode+ "&teachercode=" + teachercode+ "&status=" + status.toString();
			fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
            })
             .then(response => {
                    if (response.ok) {
                        alert('Checked');
                        window.location.reload();
                    } else {
                        alert('Failed to Check Student.');
                    }
                })
                      .catch(error => {
                    console.error('Error saving data:', error);
                    alert('Failed to save data.');
                })
            
            $(this).prop('disabled', true);
        });
    });
</script>
<script>
    $(document).ready(function() {
        var TOP = <%= data.get(12) %>;
        var DL = <%= data.get(14) %>;
        var BA = <%= data.get(15) %>;
        if (TOP >=10 && TOP <20 ) {
            $('#TOP1').show(); 
        }if (TOP >=20&& TOP <30) {
            $('#TOP2').show(); 
        }if (TOP >=30) {
            $('#TOP3').show(); 
        }if (DL >=1 && DL <3) {
            $('#DL1').show(); 
        }if (DL >=3&& DL <6) {
            $('#DL2').show(); 
        }if (DL >=6) {
            $('#DL3').show(); 
        }if (BA >=1 && BA <3) {
            $('#BA1').show(); 
        }if (BA >=3&& BA <6) {
            $('#BA2').show(); // แสดงภาพเมื่อคะแนน Attendance น้อยกว่า 1
        }if (BA >=6) {
            $('#BA3').show(); 
        }
    });
</script>
<script>
$('#studentsocreTable tbody tr').each(function() {
    var cellScore = $(this).find('td:eq(3)'); 
    var cellName = $(this).find('td:eq(2)'); 
    var cellnumb = $(this).find('td:eq(0)'); 
    var cellid = $(this).find('td:eq(1)'); 
    var name = cellName.text();console.log(name);
    var numb = cellnumb.text();
    var id = cellid.text();
    var score = parseInt(cellScore.text()); // ดึงคะแนนจากเซลล์และแปลงเป็นจำนวนเต็ม
    if (score >= 10) {
        cellName.css('background-color', 'green').html(name); 
        cellnumb.css('background-color', 'green').html(numb); 
        cellid.css('background-color', 'green').html(id); 
    }else if(score >= 5&& score < 10){
        cellName.css('background-color', 'yellow').html(name); 
         cellnumb.css('background-color', 'yellow').html(numb); 
         cellid.css('background-color', 'yellow').html(id); 
    }else if(score >= 1&& score < 5){
        cellName.css('background-color', 'orange').html(name); 
     cellnumb.css('background-color', 'orange').html(numb); 
     cellid.css('background-color', 'orange').html(id); 
}else if(score < 1){
    cellName.css('background-color', 'red').html(name); 
     cellnumb.css('background-color', 'red').html(numb); 
     cellid.css('background-color', 'red').html(id); 
}
});
</script>
<img id="TOP3" class="TOP3" src="https://img5.pic.in.th/file/secure-sv1/top3dab2a4e163f63607.png" alt="top3dab2a4e163f63607.png" border="0" />
<img id="TOP2" class="TOP2"  src="https://img5.pic.in.th/file/secure-sv1/top2.png" alt="top2.png" border="0" />
<img id="TOP1" class="TOP1" src="https://img5.pic.in.th/file/secure-sv1/top1.png" alt="top1.png" border="0" />
<img id="DL3" class="DL3" src="https://img2.pic.in.th/pic/learner3.png" alt="learner3.png" border="0" />
<img id="DL2" class="DL2" src="https://img5.pic.in.th/file/secure-sv1/learner2.png" alt="learner2.png" border="0" />
<img id="DL1" class="DL1" src="https://img5.pic.in.th/file/secure-sv1/learner1.png" alt="learner1.png" border="0" />
<img id="BA3" class="BA3" src="https://img5.pic.in.th/file/secure-sv1/BA3a988d8e39712b94c.png" alt="BA3a988d8e39712b94c.png" border="0" />
<img id="BA2" class="BA2"  src="https://img2.pic.in.th/pic/BA2.png" alt="BA2.png" border="0" />
<img id="BA1" class="BA1"  src="https://img2.pic.in.th/pic/BA1.png" alt="BA1.png" border="0" />
</html>
