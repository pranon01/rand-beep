<!DOCTYPE html>
<%@ page import="java.util.ArrayList, java.util.List" %>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Main Menu</title>
<style>
    /* CSS code for centering the form and styling */
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh; /* Set the height of the body to fill the viewport */
        margin: 0; /* Remove default margin */
        font-family: Arial, sans-serif; /* Set the font family */
    }

    .form-container {
        display: flex;
        flex-direction: column;
        align-items: center;
        max-width: 500px; /* Set the maximum width of the form container */
        padding: 20px;
        background-color: #f4f4f4;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
    }

    h1, h2 {
        text-align: center;
    }

    select, input[type="text"], input[type="password"], input[type="submit"], button {
        width: 100%;
        margin-bottom: 10px;
        padding: 10px;
        box-sizing: border-box;
        border: 1px solid #ccc;
        border-radius: 5px;
    }

    button[type="button"] {
        cursor: pointer;
        background-color: #4caf50;
        color: white;
        border: none;
    }

    button[type="button"]:hover {
        background-color: #45a049;
    }

    input[type="submit"] {
        background-color: #4caf50;
        color: white;
        border: none;
    }

    input[type="submit"]:hover {
        background-color: #45a049;
    }
</style>
</head>
<body>
    <div class="form-container">
        <form id="courseForm" action="teacher_showTable" method="post">
            <h1><%=request.getAttribute("logincode") %></h1>
            <h2>Select a Course:</h2>
            <select id="coursecode" name="coursecode">
                <% List<String> tablenames = (List<String>) request.getAttribute("tableNames");
                   if (tablenames != null) {
                       for (String name : tablenames) {
                %>
                <option value="<%= name %>"><%= name %></option>
                <% 
                       }
                   }
                %>
            </select>
            <button type="button" onclick="getSelectedCourse()">Selected Course</button>
            <input type="hidden" id="logincode" name="logincode" value="<%= request.getAttribute("logincode") %>">
        </form>

        <p id="selected"></p>

        <form action="insertCourse" method="post">
            <h2>Add Course:</h2>
            <div>
                <label for="newCourseCode">Course code:</label>
                <input type="text" id="newCourseCode" name="coursecode" required>
            </div>
            <div>
                <label for="newCourseUrl">Url:</label>
                <input type="text" id="newCourseUrl" name="url" required>
            </div>
            <input type="hidden" id="logincode" name="logincode" value="<%= request.getAttribute("logincode") %>">
            <input type="submit" value="Submit">
        </form>
    </div>

    <script>
        function getSelectedCourse() {
            var dropdown = document.getElementById("coursecode");
            var selectedOption = dropdown.options[dropdown.selectedIndex].value;
            var logincode = document.getElementById("logincode").value;
            document.getElementById("courseForm").submit();
        }
    </script>
</body>
</html>
