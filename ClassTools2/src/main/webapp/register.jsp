<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Class Tools Register</title>
<style>
    /* CSS code for centering the form */
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh; /* Set the height of the body to fill the viewport */
        margin: 0; /* Remove default margin */
        font-family: Arial, sans-serif; /* Set the font family */
    }

    form {
        width: 100%;
        max-width: 500px; /* Set the maximum width of the form */
        padding: 20px;
        background-color: #f4f4f4;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
    }

    h1 {
        text-align: center;
        margin-bottom: 20px;
    }

    table {
        width: 100%; /* Set the table width to 100% */
    }

    td {
        padding: 5px; /* Add padding to table cells for spacing */
    }

    input[type="text"],
    input[type="password"],
    input[type="submit"] {
        width: 100%; /* Set the width of input elements to fill the parent */
        margin-bottom: 10px; /* Add margin bottom for spacing */
        padding: 10px; /* Add padding to input elements */
        box-sizing: border-box; /* Include padding in input width */
        border: 1px solid #ccc;
        border-radius: 5px;
    }

    input[type="submit"] {
        background-color: #4caf50;
        color: white;
        border: none;
        cursor: pointer;
    }

    input[type="submit"]:hover {
        background-color: #45a049;
    }
</style>
</head>
<body>
	<form method="post" action="register_check">
	<h1>Class Tools.Register</h1>
    <table>
        <tr>
            <td>COURSECODE</td>
            <td><input type="text" name="coursecode"></td>
        </tr>
        <tr>
            <td>PASSWORD</td>
            <td><input type="password" name="password"></td>
        </tr>
               <tr>
            <td>CONFIRM PASSWORD</td>
            <td><input type="password" name="passwordconfirm"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="SUBMIT"></td>
        </tr>
    </table>
</form>
</body>
</html>