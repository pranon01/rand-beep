<!DOCTYPE html>
<%@ page import="java.util.ArrayList, java.util.List" %>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Main Menu</title>
<style>
    /* Global styles */
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #f4f4f4;
    }

    .container {
        max-width: 300px;
        margin: 20px auto;
        padding: 20px;
        background-color: #fff;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
    }

    h1, h2 {
        text-align: center;
    }

    /* Form styles */
    form {
        margin-bottom: 20px;
        display: flex;
        flex-direction: column;
        align-items: center; /* Center items horizontally */
    }

    select,
    input[type="text"],
    input[type="submit"],
    button {
        width: 100%;
        padding: 10px;
        margin-bottom: 10px;
        box-sizing: border-box;
        border: 1px solid #ccc;
        border-radius: 5px;
    }

    button[type="button"],
    input[type="submit"] {
        background-color: #4CAF50;
        color: white;
        border: none;
        cursor: pointer;
    }

    button[type="button"]:hover,
    input[type="submit"]:hover {
        background-color: #45a049;
    }
</style>
</head>
<body>
    <div class="container">
        <form id="courseForm" action="student_getData" method="post">
        <h1><%=request.getAttribute("logincode") %></h1>
            Select a Course:
            <select id="coursecode" name="coursecode">
                <% List<String> CourseStd = (List<String>) request.getAttribute("CourseStd");
                   if (CourseStd != null) {
                       for (String course : CourseStd) {
                %>
                <option value="<%= course %>"><%= course %></option>
                <% 
                       }
                   }
                %>        
            </select>
            Select a Teacher:
            <select id="teachercode" name="teachercode">
                <% List<String> TeacherList = (List<String>) request.getAttribute("dbName");
                   if (TeacherList != null) {
                       for (String teacher : TeacherList) {
                %>
                <option value="<%= teacher %>"><%= teacher %></option>
                <% 
                       }
                   }
                %>
            </select>
            <button type="button" onclick="getSelectedCourse()">Selected Course</button>
            <input type="hidden" id="logincode" name="logincode" value="<%= request.getAttribute("logincode") %>">
        </form>

        <p id="selected"></p>

        <form action="insertCourseStd" method="post">
            <h2>Add Course:</h2>
            <table>
                <tr>
                    <td>Course code</td>
                    <td><input type="text" name="coursecode"></td>
                </tr>
                <tr>
                    <td>Teacher code</td>
                    <td><input type="text" name="teachercode"></td>
                </tr>
                <tr>
                    <td><input type="hidden" id="logincode" name="logincode" value="<%= request.getAttribute("logincode") %>"></td>		        
                    <td><input type="submit" value="Submit" style="margin: 0 10px 0 0;"></td> <!-- Center the submit button -->
                </tr>
            </table>
        </form>
    </div>

    <script>
        function getSelectedCourse() {
            var dropdown = document.getElementById("coursecode");   
            var dropdownTeacher = document.getElementById("teachercode");
            var logincode = document.getElementById("logincode").value;
            document.getElementById("courseForm").submit();
        }
    </script>
</body>
</html>
