<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Main menu for teacher</title>
</head>
<body>
    <script type="text/javascript">
        // Function to display popup
        function displayPopup() {
        	var logincode = '<%= request.getParameter("logincode") %>'
        	var choice ='<%=request.getParameter("choice")%>'
            alert('Log in success.['+logincode+']');
        	if(choice==='1'){
        		 window.location.href = "teacher_getTable?logincode=" + logincode;
        	}else{ window.location.href = "student_getCourse?logincode=" + logincode;
        	}
        }
        window.onload = displayPopup;
    </script>
</body>
</html>