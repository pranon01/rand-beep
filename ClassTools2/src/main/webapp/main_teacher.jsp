<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Random" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Main Teacher</title>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<style>
    /* Global styles */
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #f4f4f4;
    }
    h1 {
        text-align: center;
    }
    button {
        padding: 10px 20px;
        border: none;
        background-color: #4CAF50;
        color: white;
        border-radius: 5px;
        cursor: pointer;
        margin-bottom: 10px;
    }

    button:hover {
        background-color: #45a049;
    }

    select {
        padding: 5px;
        border-radius: 5px;
        margin-bottom: 10px;
    }

    input[type="text"] {
        padding: 5px;
        border-radius: 5px;
        margin-bottom: 10px;
    }

    /* Table styles */
    table {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 20px;
    }

    th, td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: center;
    }

    th {
        background-color: #f2f2f2;
    }

    /* Popup styles */
    #checkingPopup, #loadingPopup, #loadingexport {
        display: none;
        margin-right: 10px;
    }
</style>
</head>
<h1><%=request.getParameter("coursecode") %></h1>
<body>
<div style="display: flex; align-items: center; justify-content: space-between;">
    <button onclick="randfn(); return false;">Random</button>
    <button id="checkButton" style="margin-left: 10px;">Check</button>
    <div id="checkingPopup" style="display: none; margin-right: 10px;">checking...</div> 

    <form id="leaderform" method="post" action="leader_board" style="margin-left: auto;">
        <input type="hidden" id="coursecode" name="coursecode" value="<%=request.getParameter("coursecode") %>">
        <input type="hidden" id="logincode" name="logincode" value="<%=request.getParameter("logincode") %>">
        <button type="submit" id="leaderButton">LEADER BOARD</button>
    </form>
</div>

    <table border="1" id="teacherTable">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
                <th>Attendance</th>
                <th>HW01</th>
                <th>Lab01</th>
                <th>MidLec</th>
                <th>MidLab</th>
                <th>HW02</th>
                <th>Lab02</th>
                <th>FinalLec</th>
                <th>FinalLab</th>
                <th>SUM</th>
                <th>Status</th>
                <th>DL</th>
                <th>BA</th>
            </tr>
        </thead>
        <tbody>
            <% 
                List<String[]> data = (List<String[]>) request.getAttribute("data");
                List<String[]> name= data;
                if (data != null && !data.isEmpty()) {
                    for (String[] row : data) {
            %>
            <tr>
                <% 
                
                    for(int i = 0;i< row.length; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }
                } else {
            %>
            <tr>
                <td colspan="21">No data available</td>
            </tr>
            <% 
                }
            %>
        </tbody>
    </table>
<div style="display: flex; align-items: center;">
		<button id="saveButton">Save</button><br>
    <div id="loadingPopup" style="display: none; margin-right: 10px;">
        Saving...
    </div> 
    <div style="display: flex; align-items: center;">
		<button id="export" style="margin-left: 10px;">Export File</button>
    <div id="loadingexport" style="display: none; margin-left: 10px;">
        Exporting...
    </div>    
    <script>
        $(document).ready(function() {
            $('#teacherTable').on('input', 'td[contenteditable="true"]', function() {
                var newValue = $(this).text();
                var rowIndex = $(this).closest('tr').index();
                var columnIndex = $(this).index();
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#saveButton').click(function() {
            	$('#loadingPopup').css('display', 'block');
                var updatedData = [];
                $('#teacherTable tbody tr').each(function(index, row) {
                    var rowData = [];
                    $(row).find('td').each(function() {
                        rowData.push($(this).text());
                    });
                    updatedData.push(rowData);
                });
                var logincode = '<%= request.getParameter("logincode") %>';
                var coursecode = '<%= request.getParameter("coursecode") %>';
                var url = "updateTable?logincode=" + logincode + "&coursecode=" + coursecode;
				fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(updatedData)
                })
                .then(response => {
                    if (response.ok) {
                        alert('Saved');
                        window.location.reload();
                    } else {
                        alert('Failed to save data.');
                    }
                })
                .catch(error => {
                    console.error('Error saving data:', error);
                    alert('Failed to save data.');
                })
                .finally(() => {
                    setTimeout(function() {
                        $('#loadingPopup').css('display', 'none');
                    }, 1000);
                });
            });
        });
    </script>
            <script>
    </script>
        <script>
        $(document).ready(function() {
            $('#export').click(function() {
            	$('#loadingexport').css('display', 'block');
            	var foldername = $('#boxname').val();
                var logincode = '<%= request.getParameter("logincode") %>';
                drive = document.getElementById("dir").value;
                console.log(drive);
                var coursecode = '<%= request.getParameter("coursecode") %>';
                var url = "exportTable?logincode=" + logincode + "&coursecode=" + coursecode+ "&drive=" + drive+ "&foldername=" + foldername ;
				fetch(url, {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    })
                .then(response => {
                    if (response.ok) {
                        alert('Exported');
                        window.location.reload();
                    } else {
                        alert('Failed to export data.');
                    }
                })
                .catch(error => {
                    console.error('Error export data:', error);
                    alert('Failed to export data.');
                })
            });
        });
    </script>	
    <script>
        $(document).ready(function() {
            $('#checkButton').click(function() {
            	$('#checkingPopup').css('display', 'block');
                var logincode = '<%= request.getParameter("logincode") %>';
                var coursecode = '<%= request.getParameter("coursecode") %>';
                var isChecked = true;
                var url = "check?logincode=" + logincode + "&coursecode=" + coursecode+"&isChecked="+isChecked.toString();            
				fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                })
                .then(response => {
                    if (response.ok) {
                        alert('Checking...');
                        setTimeout(showPopup, 15000);
                        
                    }  
                    else {
                        alert('Failed to Check.');
                    }
                })
                .catch(error => {
                    console.error('Error saving data:', error);
                    alert('Failed to save data.');
                })
                .finally(() => {
                    setTimeout(function() {
                        $('#loadingPopup').css('display', 'none');
                    }, 1000);
                });
            });
        });
        function showPopup() {
            alert('Time Out');
            window.location.reload();
        }
    </script>		
	    <form >
			<script>
			  function randfn() { 	    
			            <% 
			            	Random random = new Random();
			                int randomNumber = random.nextInt(name.size())+1;
			                StringBuilder combinedName = new StringBuilder();
			                combinedName.append(name.get(randomNumber)[0]).append(" ");
			                combinedName.append(name.get(randomNumber)[1]).append(" ");
			                combinedName.append(name.get(randomNumber)[2]).append(" ");
			            	%>    
			            	alert("<%= combinedName.toString() %>"); 
			            	location.reload();
			     }
			</script>
	    </form>	  
    <select id="dir"  style=" margin-left: 10px;">
        <option value="" disabled selected>Select Drive</option>
        <option value="A">A:</option>
        <option value="B">B:</option>
        <option value="C">C:</option>
        <option value="D">D:</option>
        <option value="E">E:</option>
        <option value="F">F:</option>
        <option value="G">G:</option>
        <option value="H">H:</option>
        <option value="I">I:</option>
        <option value="J">J:</option>
        <option value="K">K:</option>
        <option value="L">L:</option>
        <option value="M">M:</option>
        <option value="N">N:</option>
        <option value="O">O:</option>
        <option value="P">P:</option>
        <option value="Q">Q:</option>
        <option value="R">R:</option>
        <option value="S">S:</option>
        <option value="T">T:</option>
        <option value="U">U:</option>
        <option value="V">V:</option>
        <option value="W">W:</option>
        <option value="X">X:</option>
        <option value="Y">Y:</option>
        <option value="Z">Z:</option>
    </select>
<input type="text" id="boxname"" placeholder="file name" style="text-align: center;">
<script>
$('#teacherTable tbody tr').each(function() {
    var cellScore = $(this).find('td:eq(3)'); 
    var cellName = $(this).find('td:eq(2)'); 
    var cellnumb = $(this).find('td:eq(0)'); 
    var cellid = $(this).find('td:eq(1)'); 
    var name = cellName.text();
    var numb = cellnumb.text();
    var id = cellid.text();
    var score = parseInt(cellScore.text()); // ดึงคะแนนจากเซลล์และแปลงเป็นจำนวนเต็ม
    if (score >= 10) {
        cellName.css('background-color', 'green').html(name); 
        cellnumb.css('background-color', 'green').html(numb); 
        cellid.css('background-color', 'green').html(id); 
    }else if(score >= 5&& score < 10){
        cellName.css('background-color', 'yellow').html(name); 
         cellnumb.css('background-color', 'yellow').html(numb); 
         cellid.css('background-color', 'yellow').html(id); 
    }else if(score >= 1&& score < 5){
        cellName.css('background-color', 'orange').html(name); 
     cellnumb.css('background-color', 'orange').html(numb); 
     cellid.css('background-color', 'orange').html(id); 
}else if(score < 1){
    cellName.css('background-color', 'red').html(name); 
     cellnumb.css('background-color', 'red').html(numb); 
     cellid.css('background-color', 'red').html(id); 
}  
});
</script>
<form id="teacherform" method="post" action="student_getData">
    <input type="hidden" id="logincode" name="logincode">
    <input type="hidden" id="coursecode" name="coursecode">
    <input type="hidden" id="teachercode" name="teachercode">
</form>

<script>
$(document).ready(function() {
    // เพิ่มการกระทำเมื่อคลิกที่เซลล์ของตาราง
    $('#teacherTable').on('click', 'td[contenteditable="true"]', function() {
        var cellValue = $(this).text(); // ดึงข้อความที่เข้าสู่เซลล์
        var coursecode_ = '<%= request.getParameter("coursecode") %>';
        var parse = coursecode_.split("_");
        var coursecode=parse[1];
        var logincode = '<%= request.getParameter("logincode") %>';   
        console.log('Cell Value:', cellValue);
        console.log('coursecode:', coursecode);
        console.log('logincode:', logincode);	
        if (cellValue.length == 8) {
        // กำหนดค่าให้ input hidden และส่ง form ไปยัง servlet
        $('#teacherform #logincode').val(cellValue);
        $('#teacherform #coursecode').val(coursecode);
        $('#teacherform #teachercode').val(logincode);
        $('#teacherform').submit();
        }
    });
});
</script>

</body>
</html>
