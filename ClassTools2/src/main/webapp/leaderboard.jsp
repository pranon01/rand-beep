<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LEADER BOARD</title>
<style>
body {
        display: grid;
        place-items: center;
        height: 100vh;
        margin: 0;
    }

    .container {
        text-align: center;
    }

    .leaderboard-image {
        max-width: 100%;
    }

    .leaderboard-table {
        margin: 20px auto;
    }
    .TOP1, .TOP2, .TOP3, .DL1, .DL2, .DL3, .BA1, .BA2, .BA3 {
        max-width: 100%; /* ปรับขนาดรูปให้ไม่เกินขนาดของ div */
        height: auto; /* ให้รูปปรับขนาดตามสัดส่วน */
    }
    table {
        margin: 20px auto; /* กำหนดให้ตารางแสดงกลางจอ */
    }
</style>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function(){
        $('.TOP1, .TOP2, .TOP3, .DL1, .DL2, .DL3, .BA1, .BA2, .BA3').css('max-width', '20%');
    });
</script>
</head>
<body>
<img id="TOP3" class="TOP3" src="https://img5.pic.in.th/file/secure-sv1/top3dab2a4e163f63607.png" alt="top3dab2a4e163f63607.png" border="0" />
<table border="1" id="studentsocreTable">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <% 
                List<String[]> data = (List<String[]>) request.getAttribute("data");
                if (data != null && !data.isEmpty()) {
                    for (String[] row : data) {
                    	int sum=Integer.parseInt(row[12]);
                    	if(sum>=30){
            %>     
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %>
        </tbody>
    	</table>
<img id="TOP2" class="TOP2"  src="https://img5.pic.in.th/file/secure-sv1/top2.png" alt="top2.png" border="0" />		
<table border="1" id="studentsocreTable2">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> dataSum2 = (List<String[]>) request.getAttribute("data");
            	  if (dataSum2 != null && !dataSum2.isEmpty()) {
                  for (String[] row : dataSum2) {
                  int sum2=Integer.parseInt(row[12]);
                    	if(sum2>=20&&sum2<30){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %>    
        </tbody>
    	</table>
<img id="TOP1" class="TOP1" src="https://img5.pic.in.th/file/secure-sv1/top1.png" alt="top1.png" border="0" />	
    	<table border="1" id="studentsocreTable3">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> dataSum3 = (List<String[]>) request.getAttribute("data");
            	  if (dataSum3 != null && !dataSum3.isEmpty()) {
                  for (String[] row : dataSum3) {
                  int sum2=Integer.parseInt(row[12]);
                    	if(sum2>=10&&sum2<20){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>
<img id="DL3" class="DL3" src="https://img2.pic.in.th/pic/learner3.png" alt="learner3.png" border="0" />
    	<table border="1" id="studentsocreTable4">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> DL3 = (List<String[]>) request.getAttribute("data");
            	  if (DL3 != null && !DL3.isEmpty()) {
                  for (String[] row : DL3) {
                  int sum2=Integer.parseInt(row[14]);
                    	if(sum2>6){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>	
<img id="DL2" class="DL2" src="https://img5.pic.in.th/file/secure-sv1/learner2.png" alt="learner2.png" border="0" />
    	<table border="1" id="studentsocreTable5">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> DL2 = (List<String[]>) request.getAttribute("data");
            	  if (DL2 != null && !DL2.isEmpty()) {
                  for (String[] row : DL2) {
                  int sum2=Integer.parseInt(row[14]);
                    	if(sum2>=3&&sum2<6){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>	
<img id="DL1" class="DL1" src="https://img5.pic.in.th/file/secure-sv1/learner1.png" alt="learner1.png" border="0" />
    	<table border="1" id="studentsocreTable6">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> DL1 = (List<String[]>) request.getAttribute("data");
            	  if (DL1 != null && !DL1.isEmpty()) {
                  for (String[] row : DL1) {
                  int sum2=Integer.parseInt(row[14]);
                    	if(sum2>=1&&sum2<3){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>
<img id="BA3" class="BA3" src="https://img5.pic.in.th/file/secure-sv1/BA3a988d8e39712b94c.png" alt="BA3a988d8e39712b94c.png" border="0" />
    	<table border="1" id="studentsocreTable7">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> BA3 = (List<String[]>) request.getAttribute("data");
            	  if (BA3 != null && !BA3.isEmpty()) {
                  for (String[] row : DL1) {
                  int sum2=Integer.parseInt(row[15]);
                    	if(sum2>6){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>
<img id="BA2" class="BA2"  src="https://img2.pic.in.th/pic/BA2.png" alt="BA2.png" border="0" />
    	<table border="1" id="studentsocreTable8">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> BA2 = (List<String[]>) request.getAttribute("data");
            	  if (BA2 != null && !BA2.isEmpty()) {
                  for (String[] row : BA2) {
                  int sum2=Integer.parseInt(row[15]);
                    	if(sum2>=3&&sum2<6){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>	
<img id="BA1" class="BA1"  src="https://img2.pic.in.th/pic/BA1.png" alt="BA1.png" border="0" />
    	<table border="1" id="studentsocreTable6">
        <thead>
            <tr>
                <th>Number</th>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead> 
       <tbody> 
            <%    List<String[]> BA1 = (List<String[]>) request.getAttribute("data");
            	  if (BA1 != null && !BA1.isEmpty()) {
                  for (String[] row : BA1) {
                  int sum2=Integer.parseInt(row[14]);
                    	if(sum2>=1&&sum2<3){
            %>   
            <tr>
                <%            
                    for(int i = 0;i< 3; i++) {
                %>
                <td contenteditable="true"><%= row[i] %></td>
                <% 
                    }
                %>
            </tr>
            <% 
                    }      
                 }
                } else {
            %>
            <tr>
                <td colspan="3">No data available</td>
            </tr>
            <% 
                }
            %> 
        </tbody>
    	</table>
    	</body>

</html>