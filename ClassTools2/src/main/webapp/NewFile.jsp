<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Badge System</title>
<style>
    .badge {
        display: inline-block;
        padding: 5px 10px;
        background-color: #007bff;
        color: #fff;
        border-radius: 10px;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>
</head>
<body>
    <div id="badgeContainer"></div>

    <script>
        // Function to create a badge
        function createBadge(text, color) {
            var badge = document.createElement('div');
            badge.className = 'badge';
            badge.textContent = text;
            badge.style.backgroundColor = color;
            return badge;
        }

        // Create badges and append them to the container
        var container = document.getElementById('badgeContainer');
        container.appendChild(createBadge('New', '#28a745'));
        container.appendChild(createBadge('Updated', '#007bff'));
        container.appendChild(createBadge('Removed', '#dc3545'));
    </script>
</body>
</html>
