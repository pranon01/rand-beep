import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.mysql.cj.jdbc.result.ResultSetMetaData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;



//แสดงข้อมูลทั้งหมด
@WebServlet("/teacher_showTable")
public class teacher_showTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public teacher_showTable() {
        super();
        // TODO Auto-generated constructor stub
    }
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String logincode=request.getParameter("logincode");
		String coursecode=request.getParameter("coursecode");
		String url = "jdbc:mysql://localhost:3306/"+logincode;
	    String location=logincode+"."+coursecode;  
	    try (Connection connection = DriverManager.getConnection(url, "root", "@root")) {
	        String query = "SELECT * FROM " + location + ";";
	        Statement statement = connection.createStatement();
	        ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData metaData = (ResultSetMetaData) resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
	        List<String[]> data = new ArrayList<>();
            while (resultSet.next()) {
                String[] rowData = new String[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = resultSet.getString(i);
                }
                data.add(rowData);
            }         
       
            request.setAttribute("data", data);
            request.setAttribute("logincode", logincode);
            request.setAttribute("coursecode", coursecode);
            request.getRequestDispatcher("main_teacher.jsp").forward(request, response);

	    }catch (SQLException e) {
	        e.printStackTrace();
	    }
	    
	    }
   
}
