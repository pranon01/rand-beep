import java.sql.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
//ส่งค่าไปเพื่อแสดงบนหน้า showMenuTeacher
@WebServlet("/teacher_getTable")
public class teacher_getTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
	   protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }

	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }
   protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   String logincode=request.getParameter("logincode");
	   String url = "jdbc:mysql://localhost:3306/"+logincode;
       String username = "root";
       String password = "@root";
       try (Connection connection = DriverManager.getConnection(url, username, password)) {
           String query = "SELECT table_name FROM information_schema.tables WHERE table_schema = '"+logincode+"'";
           Statement statement = connection.createStatement();
           ResultSet resultSet = statement.executeQuery(query);
           ArrayList<String> tableNames = new ArrayList<>();
           while (resultSet.next()) {
               String tableName = resultSet.getString("table_name");
               tableNames.add(tableName);
           }
           request.setAttribute("tableNames", tableNames);
           request.setAttribute("logincode", logincode);
           request.getRequestDispatcher("showMenuTeacher.jsp").forward(request, response);
       } catch (Exception e) {
           e.printStackTrace();
       }
   }
}



