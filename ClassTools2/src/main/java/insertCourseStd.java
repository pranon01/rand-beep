import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
@WebServlet("/insertCourseStd")
public class insertCourseStd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String logincode=request.getParameter("logincode");
		String coursecode=request.getParameter("coursecode");
        String url = "jdbc:mysql://localhost:3306/login" ;
        String username = "root";
        String password = "@root";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            String query = "UPDATE login.login_table SET Coursecode = CONCAT(IFNULL(Coursecode, ''), '," + coursecode + "') WHERE Logincode = '" + logincode + "';";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
            response.getWriter().println("<script>alert('Operation successful!'); window.location.href='http://localhost:8082/classtools/mainMenu_student?logincode=" + logincode + "';</script>");
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
	}

    }

	
