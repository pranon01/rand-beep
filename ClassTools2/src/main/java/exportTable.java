import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/exportTable")
public class exportTable extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=exported_data.xlsx");
        response.setCharacterEncoding("UTF-8");
        
        String logincode = request.getParameter("logincode");
        String coursecode = request.getParameter("coursecode");
        String dir = request.getParameter("drive");
        String foldername = request.getParameter("foldername");        
        
        String url = "jdbc:mysql://localhost:3306/" + logincode;
        String location = logincode + "." + coursecode;
        File file = new File(dir+":\\"+foldername+".xlsx");
        file.createNewFile();
        try (Connection connection = DriverManager.getConnection(url, "root", "@root")) {
            String query = "SELECT * FROM " + location + ";";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Data");
            // เพิ่มคอลัมน์เป็นชื่อฟิลด์ในตาราง
            Row headerRow = sheet.createRow(0);
            int columnCount = resultSet.getMetaData().getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                Cell cell = headerRow.createCell(i - 1);
                cell.setCellValue(resultSet.getMetaData().getColumnName(i));
            }

            List<String[]> data = new ArrayList<>();
            while (resultSet.next()) {
                String[] rowData = new String[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = resultSet.getString(i);
                }
                data.add(rowData);
            }

            int rowIndex = 1; // เริ่มต้นที่แถวที่ 1 เพราะแถวที่ 0 เป็นหัวตาราง
            for (String[] rowData : data) {
                Row row = sheet.createRow(rowIndex++);
                int cellIndex = 0;
                for (String cellData : rowData) {
                    Cell cell = row.createCell(cellIndex++);
                    cell.setCellValue(cellData);
                }
            }
            try (FileOutputStream outputStream = new FileOutputStream(dir+":\\"+foldername+".xlsx")) {
                workbook.write(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
