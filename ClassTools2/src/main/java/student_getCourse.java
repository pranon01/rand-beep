import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
@WebServlet("/student_getCourse")

public class student_getCourse extends HttpServlet {
	private static final long serialVersionUID = 1L;
	   protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }

	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        processRequest(request, response);
	    }
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "jdbc:mysql://localhost:3306/login";
        String username = "root";
        String password = "@root";
 	    String logincode=request.getParameter("logincode");
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            String query = "SELECT * FROM login.login_table WHERE Logincode=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, logincode);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<String> courselist = new ArrayList<>();
            if (resultSet.next()) {
            	String getcoursecode = resultSet.getString("Coursecode");
                courselist.add(getcoursecode);
            }
            ArrayList<String> CourseStd = new ArrayList<>();
            if(courselist!=null){
            	for(String courses:courselist) {
            		if(courses!=null) {
	                String[] courseArr=courses.split(",");
	                for (String course : courseArr) {
	                	CourseStd.add(course);
	                }
                }
            }
           }
        
//-------------------------------show Database------------------------------------------------------
            String query2 = "SHOW DATABASES WHERE `Database` NOT IN ('information_schema', 'performance_schema', 'mysql', 'login');";
            Statement statement = connection.createStatement();
            ResultSet resultSet2 = statement.executeQuery(query2);
            ArrayList<String> dbName = new ArrayList<>();
            while (resultSet2.next()) {
                String databaseName = resultSet2.getString(1);         
                	dbName.add(databaseName);
            }
            request.setAttribute("CourseStd", CourseStd);
            request.setAttribute("logincode", logincode);
            request.setAttribute("dbName", dbName);
            request.getRequestDispatcher("showMenuStudent.jsp").forward(request, response); 
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

