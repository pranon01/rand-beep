import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class getdata {
    public static StringBuilder get_data(String url) {
        StringBuilder rawdata= new StringBuilder();
        try {
            //String url = "https://reg9.nu.ac.th/registrar/student_inclass_all.asp?cmd=1&order=STUDENTNAME&classid=433536&courseid=18428&acadyear=2566&semester=2&backto=home&avs916259594=11&nDesc=0";
            Document doc = Jsoup.connect(url).get();
            Element table = doc.select("table").eq(4).first();
            if (table != null) {
                Elements rows = table.select("tr");
                for (Element row : rows) {
                    Elements cells = row.select("td"); // หรือ "th" ถ้าเป็นหัวข้อ
                    for (Element cell : cells) {
                        // System.out.print(cell.text() + "\t"); // พิมพ์ข้อความในเซลล์
                        rawdata.append(cell.text()).append("\n");
                    }
                }//System.out.print(rawdata);

            } else {
                System.out.println("ไม่พบข้อมูล");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rawdata;
    }
    public static List parseText(String url) {
        StringBuilder listdata = get_data(url);

        // ข้อความที่กำหนด
        String text = listdata.toString();
        //System.out.print(text);
        // Regular Expression เพื่อค้นหาลำดับ, รหัส, และชื่อ
        String regex =  "(\\d+)\\s+(\\d{8})\\s+(.*?)\\s+(.*?)\\s+(\\d+)"; //d{8}หมายถึงตัวเลข 8 ตัว
        // สร้าง Pattern object
        Pattern pattern = Pattern.compile(regex);

        // สร้าง Matcher object
        Matcher matcher = pattern.matcher(text);
        ArrayList<String> numberList = new ArrayList<>();
        ArrayList<String> studentIdList = new ArrayList<>();
        ArrayList<String> nameList = new ArrayList<>();
        int i = 0;
        // หากพบข้อมูลตรงกับ Regular Expression
        while (matcher.find()) {
            // ดึงข้อมูลที่ต้องการจาก Group
            String number = matcher.group(1);
            String studentId = matcher.group(2);
            String name = matcher.group(3);
            // เพิ่มข้อมูลลงใน ArrayList
            numberList.add(number);
            studentIdList.add(studentId);
            nameList.add(name);
        }
        List<ArrayList> dataList;
        dataList = Arrays.asList(numberList,studentIdList,nameList);
        // คืนค่า Array ที่ได้
        return dataList;
    }
    /*public static void main(String[] args) {
        String u = "https://reg9.nu.ac.th/registrar/student_inclass_all.asp?backto=home&option=0&classid=433536&courseid=18428&acadyear=2566&semester=2&normalURL=&avs246848558=10";
        List<List<String>> dataList = getdata.parseText(u);

    }*/
}
