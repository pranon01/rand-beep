import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

@WebServlet("/check_std")
public class check_std extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	     	String statusget = request.getParameter("status");
	        boolean status = Boolean.parseBoolean(statusget);
	        String logincode = request.getParameter("logincode");
	        String coursecode = request.getParameter("coursecode");
	        String teachercode= request.getParameter("teachercode");
	        String location = teachercode + "."+"course_" + coursecode;
	        String url = "jdbc:mysql://localhost:3306/" + teachercode;
	        String username = "root";
	        String password = "@root";
	        if(status==true) {
	        try (Connection connection = DriverManager.getConnection(url, username, password)) {
	            String query = "UPDATE " + location + " SET Attendance=Attendance+1,Status=false WHERE ID=?";
	            PreparedStatement statement = connection.prepareStatement(query);
                statement.setObject(1, logincode);
	            statement.executeUpdate();
	            // Set ScheduledExecutorService to update status=false after 15 seconds
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	}
	}
}
