

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

/**
 * Servlet implementation class leader_board
 */
@WebServlet("/leader_board")
public class leader_board extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public leader_board() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String logincode=request.getParameter("logincode");
		String coursecode=request.getParameter("coursecode");
		String url = "jdbc:mysql://localhost:3306/"+logincode;
	    String location=logincode+"."+coursecode;  
	    try (Connection connection = DriverManager.getConnection(url, "root", "@root")) {
	        String query = "SELECT * FROM " + location + ";";
	        Statement statement = connection.createStatement();
	        ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData metaData = (ResultSetMetaData) resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
	        List<String[]> data = new ArrayList<>();
            while (resultSet.next()) {
                String[] rowData = new String[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = resultSet.getString(i);
                }
                data.add(rowData);
            }         
       
            request.setAttribute("data", data);
            request.setAttribute("logincode", logincode);
            request.setAttribute("coursecode", coursecode);
            request.getRequestDispatcher("leaderboard.jsp").forward(request, response);

	    }catch (SQLException e) {
	        e.printStackTrace();
	    }
	    
	    }
	}

