import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.mysql.cj.jdbc.result.ResultSetMetaData;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.google.gson.JsonElement;

@WebServlet("/updateTable")
public class updateTable extends HttpServlet {
	 private String[] columns= {"Number", "ID", "Name","Attendance","HW01","Lab01","MidLec","MidLab","HW02","Lab02","FinalLec","FinalLab","Sum","Status","DL","BA"};
	 //DL=Determined Learner BA=BOLT ARCHIVER
    private static final long serialVersionUID = 1L;
 
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
        BufferedReader reader = request.getReader();
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        String jsonData = sb.toString();
        // แปลง JSON เป็น Java Object ด้วย Gson
        Gson gson = new Gson();
        JsonArray jsonArray = gson.fromJson(jsonData, JsonArray.class);
		String logincode=request.getParameter("logincode");
		String coursecode=request.getParameter("coursecode");
    	String url = "jdbc:mysql://localhost:3306/"+logincode;
        String username = "root";
        String password = "@root";
       String location=logincode+"."+coursecode;
        	for (int row = 0; row < jsonArray.size(); row++) {
        		for (int col = 3; col<columns.length; col++) {
        			JsonElement element = jsonArray.get(row).getAsJsonArray().get(col);
        			int value=0;
        			if (element != null && !element.isJsonNull() && !element.getAsString().equals("null")){
        			   value = element.getAsInt();
        			} else {
        			    value = 0;
        			}
 try (Connection connection = DriverManager.getConnection(url, username, password)) {
                String query = "UPDATE "+location+"   SET " +columns[(col)]+" = ? WHERE Number = ?;";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setObject(1, value);
                statement.setObject(2, row+1);
                System.out.println("column="+col+"value ="+value);
                statement.executeUpdate();
                String sumquery = "UPDATE "+location+" SET Sum = COALESCE(Attendance, 0) +COALESCE(HW01, 0) + COALESCE(Lab01, 0)+ COALESCE(MidLec, 0)+ COALESCE(MidLab, 0)+"+
                        "COALESCE(HW02, 0)" +"+ COALESCE(Lab02, 0)+ COALESCE(FinalLec, 0)+ COALESCE(FinalLab, 0)+ COALESCE(DL, 0)+ COALESCE(BA, 0);";
                PreparedStatement statementsum = connection.prepareStatement(sumquery);
                statementsum.executeUpdate();
            }catch (Exception e) {
         }
        	}

    }

}
}