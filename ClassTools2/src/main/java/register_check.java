import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/register_check")
    /*
สำหรับนิสิตเท่านั้น
     */
public class register_check extends HttpServlet {
	private static final long serialVersionUID = 1L;       

    public register_check() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
        String coursecode=request.getParameter("coursecode");
        String password=request.getParameter("password");
        String passwordconfirm=request.getParameter("passwordconfirm");
        //System.out.print(coursecode+password+passwordconfirm);
if(!password.isEmpty()&&!passwordconfirm.isEmpty()&&coursecode!=null&&password.equals(passwordconfirm)) {
        String url = "jdbc:mysql://localhost:3306/login";
        try (Connection connection = DriverManager.getConnection(url, "root", "@root")) {
            String query = "INSERT INTO login.login_table(Logincode,Password,Choice)\n" +
                    "VALUES(?,?,0);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, coursecode); 
            preparedStatement.setString(2, password); 
            preparedStatement.executeUpdate();	       
            PrintWriter out = response.getWriter(); 
	        out.println("<script type=\"text/javascript\">"); 
	        out.println("alert('Register success!');"); 
	        out.println("location='login.jsp';"); 
	        out.println("</script>");  
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
	}
	}
}
