import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

@WebServlet("/student_getData")

public class student_getData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String logincode=request.getParameter("logincode");
		String coursecode=request.getParameter("coursecode");
		String teachercode=request.getParameter("teachercode");		
	     String url = "jdbc:mysql://localhost:3306/"+teachercode;
	        String username = "root";
	        String password = "@root";
	        String location=teachercode+"."+"course_"+coursecode;
	        try (Connection connection = DriverManager.getConnection(url, username, password)) {
	            String query = "SELECT * FROM " + location + " WHERE ID='"+logincode+"';";
	            PreparedStatement preparedStatement = connection.prepareStatement(query);
	            ResultSet resultSet = preparedStatement.executeQuery();
	            ArrayList<String> rowDataList = new ArrayList<>();
	            while (resultSet.next()) {
	                ResultSetMetaData metaData = resultSet.getMetaData();
	                int columnCount = metaData.getColumnCount(); 
	                for (int i = 1; i <= columnCount; i++) {
	                    Object value = resultSet.getObject(i);
	                    rowDataList.add(value.toString());		    	       
	                }
	                request.setAttribute("rowDataList", rowDataList);
	                request.setAttribute("coursecode", coursecode);
	                request.setAttribute("teachercode", teachercode);	  
	                request.getRequestDispatcher("main_student.jsp").forward(request, response);
	            }
	        }catch (Exception e) {
	        }

	        }
	}

