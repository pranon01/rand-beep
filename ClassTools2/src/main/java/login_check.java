import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/login_check")
    public class login_check extends HttpServlet {
        private static final long serialVersionUID = 1L;

        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        	System.out.println("test");
            String coursecode=request.getParameter("coursecode");
            String password=request.getParameter("password");           
            String url = "jdbc:mysql://localhost:3306/login";
            response.setContentType("text/html;charset=UTF-8");
            response.setHeader("Cache-Control", "no-cache");
            try (Connection connection = DriverManager.getConnection(url, "root", "@root")) {
                String query = "SELECT Logincode, Password,Choice FROM login_table;";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    String logincode = resultSet.getString("Logincode");
                    String passwordfromsql = resultSet.getString("Password");
                    Integer choice = resultSet.getInt("Choice");
                    if (password.equals(passwordfromsql) && coursecode.equals(logincode)) {            
                        if (choice.equals(1)) {	               
                        	response.sendRedirect("wellcomepage.jsp?logincode=" + logincode+"&choice="+choice);            
                        }
                        if (choice.equals(0)) {
                        	response.sendRedirect("wellcomepage.jsp?logincode=" + logincode);
                        }
                         return;

                    }               
                }
                response.getWriter().println("<script>alert('Incorrect Course code or Password'); window.location.href='http://localhost:8082/classtools/login.jsp';</script>");

             }catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

