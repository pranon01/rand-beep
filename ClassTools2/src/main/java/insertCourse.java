import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;


@WebServlet("/insertCourse")
public class insertCourse extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String logincode=request.getParameter("logincode");
		String coursecode=request.getParameter("coursecode");
		String urlReg=request.getParameter("url");
		        String url = "jdbc:mysql://localhost:3306/"+logincode;
		        String username = "root";
		        String password = "@root";
		        try (Connection connection = DriverManager.getConnection(url, username, password)) {
		            String query = "CREATE TABLE IF NOT EXISTS course_" + coursecode + "("
		                    + "Number VARCHAR(50) DEFAULT 0,  ID VARCHAR(50) DEFAULT 0,Name VARCHAR(50) DEFAULT 0,  Attendance INT DEFAULT 0,  HW01 INT DEFAULT 0,LAb01 INT DEFAULT 0,"
		                    + "MidLec INT DEFAULT 0,MidLab INT DEFAULT 0,HW02 INT DEFAULT 0,Lab02 INT DEFAULT 0,FinalLec INT DEFAULT 0,FinalLab INT DEFAULT 0,Sum INT DEFAULT 0,Status TINYINT DEFAULT 0,DL INT DEFAULT 0,BA INT DEFAULT 0);";
		            PreparedStatement preparedStatement = connection.prepareStatement(query);
		            preparedStatement.executeUpdate();
		            addliststudent(coursecode,logincode,urlReg);
		            addcourse(coursecode,logincode);
		            response.getWriter().println("<script>alert('Operation successful!'); window.location.href='http://localhost:8082/classtools/teacher_getTable?logincode=" + logincode + "';</script>");
		            //System.out.println("created);
		        } catch (SQLException e) {
		            e.printStackTrace();
		        }
		    }
		    public void addcourse(String coursecode,String logincode){
		        String url = "jdbc:mysql://localhost:3306/login" ;
		        String username = "root";
		        String password = "@root";
		        try (Connection connection = DriverManager.getConnection(url, username, password)) {
		            String query = "UPDATE login.login_table SET Coursecode = CONCAT(IFNULL(Coursecode, ''), '," + coursecode + "') WHERE Logincode = '" + logincode + "';";
		            PreparedStatement preparedStatement = connection.prepareStatement(query);
		            preparedStatement.executeUpdate();
		        }
		        catch(SQLException e) {
		            e.printStackTrace();
		        }
		    }
		    public  void addliststudent(String coursecodeText,String logincode,String urlregText) {
		    String tablename = "course_"+coursecodeText;
		        String url = "jdbc:mysql://localhost:3306/"+logincode;
		        String username = "root";
		        String password = "@root";
		        List<List<Integer>> gd = getdata.parseText(urlregText);
		        int gdsize = gd.get(1).size();//หาขนาดของข้อมูล		 
		        for (int i = 0; i < gdsize; i++) {
		            Object number = gd.get(0).get(i);
		            Object StudentId = gd.get(1).get(i);
		            Object name = gd.get(2).get(i);
		            try (Connection connection = DriverManager.getConnection(url, username, password)) {
		                String query = "INSERT INTO "+tablename+"(Number, ID, Name) VALUES (" + number + ", " + StudentId + ", '" + name + "')";
		                PreparedStatement statement = connection.prepareStatement(query);
		                statement.executeUpdate();       
		            }catch (SQLException e) {
		                e.printStackTrace();
		            }
		        }
		    }
	}

