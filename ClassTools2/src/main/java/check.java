import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Servlet implementation class check
 */
@WebServlet("/check")
public class check extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String isCheckedParam = request.getParameter("isChecked");
        boolean isChecked = Boolean.parseBoolean(isCheckedParam);
        String logincode = request.getParameter("logincode");
        String coursecode = request.getParameter("coursecode");
        String location = logincode + "." + coursecode;
        String url = "jdbc:mysql://localhost:3306/" + logincode;
        String username = "root";
        String password = "@root";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            String query = "UPDATE " + location + " SET Status=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setBoolean(1, isChecked);
            statement.executeUpdate();
            // Set ScheduledExecutorService to update status=false after 15 seconds
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            public void run() {
            	try (Connection connection2 = DriverManager.getConnection(url, username, password)) {
                    String query2 = "UPDATE " + location + " SET Status=false";
                    PreparedStatement statement2 = connection2.prepareStatement(query2);
                    statement2.executeUpdate();
                    
                    // Set ScheduledExecutorService to update status=false after 15 seconds
          
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };timer.schedule(task, 15000);
    }
    
    }

